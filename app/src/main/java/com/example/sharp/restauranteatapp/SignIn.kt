package com.example.sharp.restauranteatapp

import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.SyncStateContract
import android.support.annotation.NonNull
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.sharp.restauranteatapp.Models.Users
import com.example.sharp.restauranteatapp.utils.Constants
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.analytics.FirebaseAnalytics

import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider

open class SignIn : AppCompatActivity() {
    private var mFirebaseAnalytics: FirebaseAnalytics? = null
    private var mAuth: FirebaseAuth? = null
    var TAG: String? = "SignIn"
    var RequestSignInCode: Int? = 7
    var googleApiClient: GoogleApiClient? = null
    //var SignOutButton: Button? = null
    var signInButton: SignInButton? = null
    var LoginUserName: TextView? = null
    var LoginUserEmail: TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        signInButton = findViewById(R.id.sign_in_button) as SignInButton
       // SignOutButton = findViewById(R.id.sign_out) as Button
       // LoginUserName = findViewById(R.id.textViewName) as TextView
       // LoginUserEmail = findViewById(R.id.textViewEmail) as TextView
        signInButton = findViewById(R.id.sign_in_button) as SignInButton
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mAuth = FirebaseAuth.getInstance()
         LoginUserEmail?.setVisibility(View.GONE)
         LoginUserName?.setVisibility(View.GONE)
        val googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail().requestProfile()
                .build()
        googleApiClient = GoogleApiClient.Builder(this@SignIn)
                .enableAutoManage(this@SignIn, object : GoogleApiClient.OnConnectionFailedListener {
                    override fun onConnectionFailed(@NonNull connectionResult: ConnectionResult) {
                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)

                .build()
        signInButton?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                UserSignInMethod()
            }
        })
//        SignOutButton?.setOnClickListener(object : View.OnClickListener {
//            override fun onClick(view: View) {
//                UserSignOutFunction()
//            }
//        })
    }

    fun UserSignInMethod() {
        // Passing Google Api Client into Intent.
        var AuthIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient)
        startActivityForResult(AuthIntent, RequestSignInCode!!)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestSignInCode) {
            val googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
             if (googleSignInResult.isSuccess()) {
            val googleSignInAccount = googleSignInResult.getSignInAccount()
                 var intent=Intent(this@SignIn,NavigationDrawerActivity::class.java)
                 startActivity(intent)
            FirebaseUserAuth(googleSignInAccount)

             }
        }else{
            Toast.makeText(applicationContext,"Login Failed",Toast.LENGTH_LONG).show()
        }
    }

    fun FirebaseUserAuth(googleSignInAccount: GoogleSignInAccount?) {
        val authCredential = GoogleAuthProvider.getCredential(googleSignInAccount?.getIdToken(), null)
        Toast.makeText(this@SignIn, "" + authCredential.getProvider(), Toast.LENGTH_LONG).show()
        mAuth?.signInWithCredential(authCredential)
                ?.addOnCompleteListener(this@SignIn, object : OnCompleteListener<AuthResult> {
                    override fun onComplete(@NonNull AuthResultTask: Task<AuthResult>) {
                        if(BuildConfig.DEBUG) Log.d(TAG,"SignInWithCredential:onComplete:" +AuthResultTask.isSuccessful)
                        if (AuthResultTask.isSuccessful()) {
                            // Getting Current Login user details.
                            var photoUrl:String? = null
                            var firebaseUser = mAuth?.getCurrentUser()

                            if(googleSignInAccount?.getPhotoUrl()!=null){
                                photoUrl = googleSignInAccount?.getPhotoUrl().toString()
                            }
                            var user =  Users ()
                                googleSignInAccount?.getDisplayName() + "" + googleSignInAccount?.getFamilyName()
                                googleSignInAccount?.getEmail()
                                photoUrl

                                FirebaseAuth.getInstance().getCurrentUser()?.getUid()
                           var database =FirebaseAnalytics.getInstance(this@SignIn)
                         // var userRef = database.setUserId(Constants).toString(Co)

                                 //SignOutButton?.setVisibility(View.VISIBLE)
                            // Hiding Login in button.
                            signInButton?.setVisibility(View.GONE)
                            // Showing the TextView.
                            LoginUserEmail?.setVisibility(View.VISIBLE)
                            LoginUserName?.setVisibility(View.VISIBLE)
                            // Setting up name into TextView.
                            LoginUserName?.setText("NAME = " + firebaseUser?.getDisplayName().toString())
                            // Setting up Email into TextView.
                            LoginUserEmail?.setText("Email = " + firebaseUser?.getEmail().toString())
                        } else {
                            Toast.makeText(this@SignIn, "Something Went Wrong", Toast.LENGTH_LONG).show()
                        }
                    }
                })
    }

//    fun UserSignOutFunction() {
//
//        mAuth?.signOut()
//        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
//                object : ResultCallback<Status> {
//                    override fun onResult(@NonNull status: Status) {
//                        // Write down your any code here which you want to execute After Sign Out.
//                        // Printing Logout toast message on screen.
//                        Toast.makeText(this@SignIn, "Logout Successfully", Toast.LENGTH_LONG).show()
//                    }
//                })
//        // After logout Hiding sign out button.
//        SignOutButton?.setVisibility(View.GONE)
//        // After logout setting up email and name to null.
//        LoginUserName?.setText(null)
//        LoginUserEmail?.setText(null)
//        // After logout setting up login button visibility to visible.
//        signInButton?.setVisibility(View.VISIBLE)
//    }
}




