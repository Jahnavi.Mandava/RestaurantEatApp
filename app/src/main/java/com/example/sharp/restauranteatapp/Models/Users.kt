package com.example.sharp.restauranteatapp.Models

class Users {
    var user:String?=null
    var email:String?=null
    var photoUrl:String?=null
    var Uid:String?=null
    fun Users() {
    }
    fun Users(user:String) {
        this.user = user
    }
    fun Users(user: String,email:String,photoURL:String,uid:String){
        this.user = user
        this.email=email
        this.photoUrl=photoUrl
        Uid=uid
    }
internal    fun getUser():String? {
        return user
    }
   internal  fun setUser(user:String) {
        this.user = user
    }
    internal    fun getEmail():String? {
        return email
    }
    internal  fun setEmail(user:String) {
        this.email = email
    }
    internal    fun getPhotoUrl():String? {
        return photoUrl
    }
    internal  fun setPhotoUrl(user:String) {
        this.photoUrl = photoUrl
    }
    internal    fun getUid():String? {
        return Uid
    }
    internal  fun setUid(user:String) {
        this.Uid = Uid
    }

}